variable "region" {
  default = "eu-west-1"
}

variable "ami_inst_type_bastion" {
  default = "t2.micro"
}

variable "source_cidrs" {
  type = "list"
  default = ["84.40.78.44/32"]
}

variable "db_port" {
  default = "3306"
}

variable "environment_tag" {
  default = "test"
}

variable "deployment_method" {
  default = "terraform"
}
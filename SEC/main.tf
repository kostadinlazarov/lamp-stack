provider "aws" {
  version = ">= 1.0.0"
  region  = "${var.region}"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "VPC/terraform.tfstate"
    region = "eu-west-1"
  }
}

terraform {
  backend "s3" {
    bucket = "example-infra-terraform-states"
    key    = "SEC/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ami" "amazon_linux" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

#####################################  Resources  #####################################

resource "random_string" "db_password" {
  length = 16
  min_upper = 2
  min_lower = 2
  min_numeric  = 2
  special = false
}

resource "aws_ssm_parameter" "db_password_ssm" {
  name        = "${var.environment_tag}-database-password"
  description = "${var.environment_tag} database master password"
  type        = "SecureString"
  value       = "${random_string.db_password.result}"

  tags {
    Name       = "${var.environment_tag}-db-master-password"
    Deployment = "${var.deployment_method}"
  }
}

resource "tls_private_key" "web_priv_ec2_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "web_pub_ec2_key" {
  key_name   = "${var.environment_tag}-web-public-ec2-key"
  public_key = "${tls_private_key.web_priv_ec2_key.public_key_openssh}"
}

output "web_ec2_key" {
  value = "${aws_key_pair.web_pub_ec2_key.key_name}"
}

resource "aws_ssm_parameter" "web_priv_ec2_key_ssm" {
  name        = "${var.environment_tag}-web-priv-ec2-key"
  description = "${var.environment_tag} web private ec2 key"
  type        = "SecureString"
  value       = "${tls_private_key.web_priv_ec2_key.private_key_pem}"

  tags {
    Name       = "${var.environment_tag}-web-priv-ec2-key"
    Deployment = "${var.deployment_method}"
  }
}

resource "tls_private_key" "bastion_priv_ec2_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "bastion_pub_ec2_key" {
  key_name   = "${var.environment_tag}-bastion-public-ec2-key"
  public_key = "${tls_private_key.bastion_priv_ec2_key.public_key_openssh}"
}

resource "aws_ssm_parameter" "bastion_priv_ec2_key_ssm" {
  name        = "${var.environment_tag}-bastion-priv-ec2-key"
  description = "${var.environment_tag} bastion private ec2 key"
  type        = "SecureString"
  value       = "${tls_private_key.bastion_priv_ec2_key.private_key_pem}"

  tags {
    Name       = "${var.environment_tag}-bastion-priv-ec2-key"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_security_group" "bastion_sg" {
  name        = "${var.environment_tag}-bastion-sg"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  tags {
    Name       = "${var.environment_tag}-bastion-sg"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_security_group_rule" "bastion_allow_ingress" {
  type                     = "ingress"
  from_port                = "22"
  protocol                 = "tcp"
  to_port                  = "22"
  cidr_blocks              = ["${var.source_cidrs}"]
  security_group_id        = "${aws_security_group.bastion_sg.id}"
}

resource "aws_security_group_rule" "bastion_allow_egress" {
  type                     = "egress"
  from_port                = "0"
  protocol                 = "-1"
  to_port                  = "0"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = "${aws_security_group.bastion_sg.id}"
}

output "bastion_sg" {
  value = "${aws_security_group.bastion_sg.id}"
}

resource "aws_security_group" "elb_sg" {
  name        = "${var.environment_tag}-elb-sg"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  tags {
    Name       = "${var.environment_tag}-elb-sg"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_security_group_rule" "elb_allow_ingress" {
  type                     = "ingress"
  from_port                = "80"
  protocol                 = "tcp"
  to_port                  = "80"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = "${aws_security_group.elb_sg.id}"
}

resource "aws_security_group_rule" "elb_allow_egress" {
  type                     = "egress"
  from_port                = "80"
  protocol                 = "tcp"
  to_port                  = "80"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = "${aws_security_group.elb_sg.id}"
}

output "elb_sg" {
  value = "${aws_security_group.elb_sg.id}"
}

resource "aws_security_group" "web_sg" {
  name        = "${var.environment_tag}-web-sg"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  tags {
    Name       = "${var.environment_tag}-web-sg"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_security_group_rule" "web_allow_https_ingress" {
  type                     = "ingress"
  from_port                = "80"
  protocol                 = "tcp"
  to_port                  = "80"
  source_security_group_id = "${aws_security_group.elb_sg.id}"
  security_group_id        = "${aws_security_group.web_sg.id}"
}

resource "aws_security_group_rule" "web_allow_ssh_ingress" {
  type                     = "ingress"
  from_port                = "22"
  protocol                 = "tcp"
  to_port                  = "22"
  source_security_group_id = "${aws_security_group.bastion_sg.id}"
  security_group_id        = "${aws_security_group.web_sg.id}"
}

resource "aws_security_group_rule" "web_allow_egress" {
  type                     = "egress"
  from_port                = "0"
  protocol                 = "tcp"
  to_port                  = "0"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = "${aws_security_group.web_sg.id}"
}

output "web_sg" {
  value = "${aws_security_group.web_sg.id}"
}

resource "aws_security_group" "db_sg" {
  name        = "${var.environment_tag}-db-sg"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  tags {
    Name       = "${var.environment_tag}-db-sg"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_security_group_rule" "rds_allow_ingress" {
  type                     = "ingress"
  from_port                = "${var.db_port}"
  protocol                 = "tcp"
  to_port                  = "${var.db_port}"
  source_security_group_id = "${aws_security_group.web_sg.id}"
  security_group_id        = "${aws_security_group.db_sg.id}"
}

output "db_sg" {
  value = "${aws_security_group.db_sg.id}"
}

resource "aws_eip" "eip_bastion" {
  vpc = true

  tags {
    Name       = "${var.environment_tag}-eip-bastion"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_iam_role" "iamrole_bastion" {
  name = "${var.environment_tag}-bastion-iam-role"

  assume_role_policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Sid":"",
         "Effect":"Allow",
         "Principal":{
            "Service":["ec2.amazonaws.com"]
         },
         "Action":"sts:AssumeRole"
      }
   ]
}
EOF
}

resource "aws_iam_policy" "iampolicy_bastion" {
  name        = "${var.environment_tag}-bastion-iam-policy"
  description = "IAM policy for the Bastion host"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
                "Action": "ec2:AssociateAddress",
                "Effect": "Allow",
                "Resource": "*"
      }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "role_policy_attach_bastion" {
  role       = "${aws_iam_role.iamrole_bastion.name}"
  policy_arn = "${aws_iam_policy.iampolicy_bastion.arn}"
}

resource "aws_iam_instance_profile" "iamprofile_bastion" {
  name = "${var.environment_tag}-bastion-iamprofile"
  role = "${aws_iam_role.iamrole_bastion.id}"
}

resource "aws_launch_configuration" "lc_bastion" {
  depends_on           = ["aws_security_group.bastion_sg"]
  name_prefix          = "${var.environment_tag}-lc-bastion"
  iam_instance_profile = "${aws_iam_instance_profile.iamprofile_bastion.id}"
  image_id             = "${data.aws_ami.amazon_linux.id}"
  instance_type        = "${var.ami_inst_type_bastion}"
  key_name             = "${aws_key_pair.bastion_pub_ec2_key.key_name}"
  security_groups      = ["${aws_security_group.bastion_sg.id}"]

  user_data = <<EOF
#!/bin/bash
sudo yum update -y

INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | grep region | awk -F\" '{print $4}')
EIP=${aws_eip.eip_bastion.id}

aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $EIP --allow-reassociation --region $REGION
EOF

  associate_public_ip_address = true

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg_bastion" {
  depends_on                = ["aws_launch_configuration.lc_bastion"]
  name_prefix               = "${var.environment_tag}-asg-bastion}"
  availability_zones        = ["${data.terraform_remote_state.vpc.availability_zones}"]
  vpc_zone_identifier       = ["${data.terraform_remote_state.vpc.public_subnets_ids}"]
  launch_configuration      = "${aws_launch_configuration.lc_bastion.id}"
  max_size                  = "1"
  min_size                  = "1"
  desired_capacity          = "1"
  health_check_grace_period = "300"

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "${var.environment_tag}-bastion-ec2"
  }

  tag {
    key                 = "Deployment"
    propagate_at_launch = true
    value               = "${var.deployment_method}"
  }
}

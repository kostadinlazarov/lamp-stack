provider "aws" {
  version = ">= 1.0.0"
  region  = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "example-infra-terraform-states"
    key    = "VPC/terraform.tfstate"
    region = "eu-west-1"
  }
}

#####################################  Resources  #####################################

resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags {
    Name          = "${var.environment_tag}-vpc"
    Deployment    = "${var.deployment_method}"
  }
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "availability_zones" {
  value = ["${var.availability_zones}"]
}

resource "aws_subnet" "public_subnet_1a" {
  depends_on        = ["aws_internet_gateway.igw"]
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.public_subnets_cidrs[0]}"
  availability_zone = "${var.availability_zones[0]}"
  map_public_ip_on_launch = true

  tags{
    Name          = "${var.environment_tag}-public-1a"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_subnet" "public_subnet_1b" {
  depends_on        = ["aws_internet_gateway.igw"]
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.public_subnets_cidrs[1]}"
  availability_zone = "${var.availability_zones[1]}"
  map_public_ip_on_launch = true

  tags{
    Name          = "${var.environment_tag}-public-1b"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_subnet" "web_subnet_1a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.web_subnets_cidrs[0]}"
  availability_zone = "${var.availability_zones[0]}"
  map_public_ip_on_launch = false

  tags{
    Name          = "${var.environment_tag}-web-private-1a"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_subnet" "web_subnet_1b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.web_subnets_cidrs[1]}"
  availability_zone = "${var.availability_zones[1]}"
  map_public_ip_on_launch = false

  tags{
    Name          = "${var.environment_tag}-web-private-1b"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_subnet" "db_subnet_1a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.db_subnets_cidrs[0]}"
  availability_zone = "${var.availability_zones[0]}"
  map_public_ip_on_launch = false

  tags{
    Name          = "${var.environment_tag}-db-private-1a"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_subnet" "db_subnet_1b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${var.db_subnets_cidrs[1]}"
  availability_zone = "${var.availability_zones[1]}"
  map_public_ip_on_launch = false

  tags{
    Name          = "${var.environment_tag}-bd-private-1b"
    Deployment    = "${var.deployment_method}"
  }
}

output "public_subnets_ids" {
  value = ["${aws_subnet.public_subnet_1a.id}", "${aws_subnet.public_subnet_1b.id}"]
}

output "public_subnets_cidrs" {
  value = ["${var.public_subnets_cidrs}"]
}

output "web_subnets_ids" {
  value = ["${aws_subnet.web_subnet_1a.id}", "${aws_subnet.web_subnet_1b.id}"]
}

output "web_subnets_cidrs" {
  value = ["${var.web_subnets_cidrs}"]
}

output "db_subnets_ids" {
  value = ["${aws_subnet.db_subnet_1a.id}", "${aws_subnet.db_subnet_1b.id}"]
}

output "db_subnets_cidrs" {
  value = ["${var.db_subnets_cidrs}"]
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name          = "${var.environment_tag}-vpc"
    Deployment    = "${var.deployment_method}"
  }
}

output "igw_id" {
  value = "${aws_internet_gateway.igw.id}"
}

resource "aws_eip" "eip_natgw_1a" {
  vpc = true
  depends_on = ["aws_internet_gateway.igw"]

  tags{
    Name          = "${var.environment_tag}-eip-natgw-1a"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_eip" "eip_natgw_1b" {
  vpc = true
  depends_on = ["aws_internet_gateway.igw"]

  tags{
    Name          = "${var.environment_tag}-eip-natgw-1b"
    Deployment    = "${var.deployment_method}"
  }
}

resource "aws_nat_gateway" "natgw_1a" {
  allocation_id = "${aws_eip.eip_natgw_1a.id}"
  subnet_id     = "${aws_subnet.public_subnet_1a.id}"
  depends_on    = ["aws_internet_gateway.igw"]

  tags {
    Name          = "${var.environment_tag}-natgw-1a"
    Deployment    = "${var.deployment_method}"
  }
}

output "natgw_1a_id" {
  value = "${aws_nat_gateway.natgw_1a.id}"
}

resource "aws_nat_gateway" "natgw_1b" {
  allocation_id = "${aws_eip.eip_natgw_1b.id}"
  subnet_id     = "${aws_subnet.public_subnet_1b.id}"
  depends_on    = ["aws_internet_gateway.igw"]

  tags {
    Name          = "${var.environment_tag}-natgw-1b"
    Deployment    = "${var.deployment_method}"
  }
}

output "natgw_1b_id" {
  value = "${aws_nat_gateway.natgw_1b.id}"
}

resource "aws_route_table" "public_rt" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name          = "${var.environment_tag}-public-rt"
    Deployment    = "${var.deployment_method}"
  }
}

output "public_rt_id" {
  value = "${aws_route_table.public_rt.id}"
}

resource "aws_route" "default_public_route" {
  route_table_id         = "${aws_route_table.public_rt.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.igw.id}"
}

resource "aws_route_table_association" "public_subnet_1a_rt_association" {
  subnet_id      = "${aws_subnet.public_subnet_1a.id}"
  route_table_id = "${aws_route_table.public_rt.id}"
}

resource "aws_route_table_association" "public_subnet_1b_rt_association" {
  subnet_id      = "${aws_subnet.public_subnet_1b.id}"
  route_table_id = "${aws_route_table.public_rt.id}"
}

resource "aws_route_table" "web_private_rt_1a" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name          = "${var.environment_tag}-web-private-rt-1a"
    Deployment    = "${var.deployment_method}"
  }
}

output "web_private_rt_1a_id" {
  value = "${aws_route_table.web_private_rt_1a.id}"
}

resource "aws_route_table" "web_private_rt_1b" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name          = "${var.environment_tag}-web-private-rt-1b"
    Deployment    = "${var.deployment_method}"
  }
}

output "web_private_rt_1b_id" {
  value = "${aws_route_table.web_private_rt_1b.id}"
}

resource "aws_route_table_association" "web_priv_1a_rt_association" {
  subnet_id      = "${aws_subnet.web_subnet_1a.id}"
  route_table_id = "${aws_route_table.web_private_rt_1a.id}"
}

resource "aws_route_table_association" "web_priv_1b_rt_association" {
  subnet_id      = "${aws_subnet.web_subnet_1b.id}"
  route_table_id = "${aws_route_table.web_private_rt_1b.id}"
}

resource "aws_route" "default_web_priv_route_1a" {
  route_table_id         = "${aws_route_table.web_private_rt_1a.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.natgw_1a.id}"
}

resource "aws_route" "default_web_priv_route_1b" {
  route_table_id         = "${aws_route_table.web_private_rt_1b.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.natgw_1b.id}"
}
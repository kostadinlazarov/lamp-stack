variable "region" {
  default = "eu-west-1"
}

variable "availability_zones" {
  type = "list"
  default = ["eu-west-1a","eu-west-1b"]

  type = "list"
}

variable "vpc_cidr" {
  default = "10.0.0.0/19"
}

variable "public_subnets_cidrs" {
  type = "list"
  default = ["10.0.0.0/24","10.0.1.0/24"]
}

variable "web_subnets_cidrs" {
  type = "list"
  default = ["10.0.10.0/24","10.0.11.0/24"]
}

variable "db_subnets_cidrs" {
  type = "list"
  default = ["10.0.20.0/24","10.0.21.0/24"]
}

variable "environment_tag" {
  default = "test"
}

variable "deployment_method" {
  default = "terraform"
}
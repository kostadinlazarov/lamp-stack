# LAMP infrastructure repository

---

This repository will host the code that will be used to build the infrastructure for LAMP project.

## Initial requirements

---

It is required that S3 bucket (example-infra-terraform-states) is created prior to executing terraform for the first time.
This bucket will store the remote terraform state files that list sensible information, like passwords, certificates and keys, in plain text.
It is advised to have strong access control over its content!

## Solution diagram

---

![DIAGRAM](https://www.lucidchart.com/publicSegments/view/055e71a4-8c45-4890-91ce-64259b76793e/image.jpeg)

## Repository structure

---

Repository consist of four separate folders.
Each folder hosts at least two terraform files main.tf and var.tf
The var file contains all the variables with their respective values.
Then main file hosts the rest of terraform object types.
A detailed list of Directories and files is listed below:

| Folder name | main.tf | var.tf | *.sh |
| --------------|------|-----|----- |
| VPC | X | X | - |
| SEC | X | X | - |
| DB | X | X | - |
| WEB | X | X | X |

It is required to use terraform init upon the first deployment!
The order in which templates need to be deployed is as follows:

* Networking(VPC):
    1. terraform init *(upon first deployment)*
    2. terraform plan
    3. terraform apply
* Security(SEC):
    1. terraform init *(upon first deployment)*
    2. terraform plan
    3. terraform apply
* Database(DB):
    1. terraform init *(upon first deployment)*
    2. terraform plan
    3. terraform apply
* Web(WEB):
    1. terraform init *(upon first deployment)*
    2. terraform plan
    3. terraform apply

## Future improvements

---

Set alarms and auto-scaling policy for adjusting web layer capacity.
Add CloudWatch metrics and alarms for monitoring DB, WEB, ELB IOPS performance.
Configure automated snapshots for EBS volumes and DB instance.
Add centralize security access management by deploying Simple AD.

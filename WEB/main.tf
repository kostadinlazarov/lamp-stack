provider "aws" {
  version = ">= 1.0.0"
  region  = "${var.region}"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "VPC/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "sec" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "SEC/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "db" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "DB/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ami" "amazon_linux" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

data "aws_iam_policy" "AmazonSSMReadOnlyAccess" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
}

data "template_file" "user_data" {
  template = "${file("${path.module}/user-data.sh")}"
}

terraform {
  backend "s3" {
    bucket = "example-infra-terraform-states"
    key    = "WEB/terraform.tfstate"
    region = "eu-west-1"
  }
}

#####################################  Resources  #####################################

resource "aws_iam_role" "iamrole_web" {
  name = "${var.environment_tag}-web-iam-role"

  assume_role_policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Sid":"",
         "Effect":"Allow",
         "Principal":{
            "Service":["ec2.amazonaws.com"]
         },
         "Action":"sts:AssumeRole"
      }
   ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "role_policy_attach_web" {
  role       = "${aws_iam_role.iamrole_web.name}"
  policy_arn = "${data.aws_iam_policy.AmazonSSMReadOnlyAccess.arn}"
}

resource "aws_iam_instance_profile" "iamprofile_web" {
  name = "${var.environment_tag}-web-iamprofile"
  role = "${aws_iam_role.iamrole_web.id}"
}

resource "aws_launch_configuration" "lc_web" {
  name_prefix          = "${var.environment_tag}-lc-web"
  iam_instance_profile = "${aws_iam_instance_profile.iamprofile_web.id}"
  image_id             = "${data.aws_ami.amazon_linux.id}"
  instance_type        = "${var.ami_inst_type_web}"
  key_name             = "${data.terraform_remote_state.sec.web_ec2_key}"
  security_groups      = ["${data.terraform_remote_state.sec.web_sg}"]
  user_data            = "${data.template_file.user_data.rendered}"

  associate_public_ip_address = false

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg_web" {
  depends_on                = ["aws_launch_configuration.lc_web"]
  name_prefix               = "${var.environment_tag}-asg-web}"
  availability_zones        = ["${data.terraform_remote_state.vpc.availability_zones}"]
  vpc_zone_identifier       = ["${data.terraform_remote_state.vpc.web_subnets_ids}"]
  launch_configuration      = "${aws_launch_configuration.lc_web.id}"
  max_size                  = "2"
  min_size                  = "2"
  desired_capacity          = "2"
  health_check_grace_period = "300"
  health_check_type         = "ELB"

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "${var.environment_tag}-web-ec2"
  }

  tag {
    key                 = "Deployment"
    propagate_at_launch = true
    value               = "${var.deployment_method}"
  }
}

resource "aws_elb" "web-elb" {
  name = "${var.environment_tag}-elb"
  internal = false
  security_groups = ["${data.terraform_remote_state.sec.elb_sg}"]
  cross_zone_load_balancing   = true
  subnets = ["${data.terraform_remote_state.vpc.public_subnets_ids}"]

  listener {
    instance_port     = "80"
    instance_protocol = "http"
    lb_port           = "80"
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:80"
    interval            = "30"
  }
}

resource "aws_autoscaling_attachment" "web_asg_attachment" {
  autoscaling_group_name = "${aws_autoscaling_group.asg_web.id}"
  elb                    = "${aws_elb.web-elb.id}"
}

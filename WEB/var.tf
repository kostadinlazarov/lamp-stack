variable "region" {
  default = "eu-west-1"
}

variable "ami_inst_type_web" {
  default = "t2.micro"
}

variable "environment_tag" {
  default = "test"
}

variable "deployment_method" {
  default = "terraform"
}
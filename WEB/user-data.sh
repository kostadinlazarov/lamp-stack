#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install nginx1.12 -y

sudo cat <<EOF > /usr/share/nginx/html/index.html
<!doctype html>
<html>
  <head>
    <title>Simple index page</title>
  </head>
  <body>
    <center>
    <h2>Hello
    <br>John Doe</h2>
    </center>
  </body>
</html>
EOF

sudo service nginx start
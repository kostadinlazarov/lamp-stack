variable "region" {
  default = "eu-west-1"
}

variable "db_user" {
  default = "adminstrator"
}

variable "db_name" {
  default = "mysqldb"
  # Must begin with a letter and contain only alphanumeric characters.
}

variable "db_port" {
  default = "3306"
}

variable "environment_tag" {
  default = "test"
}

variable "deployment_method" {
  default = "terraform"
}

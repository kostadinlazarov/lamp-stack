provider "aws" {
  version = ">= 1.0.0"
  region  = "${var.region}"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "VPC/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "sec" {
  backend = "s3"

  config {
    bucket = "example-infra-terraform-states"
    key    = "SEC/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ssm_parameter" "db_password" {
  name = "test-database-password"
}

terraform {
  backend "s3" {
    bucket = "example-infra-terraform-states"
    key    = "DB/terraform.tfstate"
    region = "eu-west-1"
  }
}

#####################################  Resources  #####################################

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.environment_tag}-db-subnet-group"
  subnet_ids = ["${data.terraform_remote_state.vpc.db_subnets_ids}"]

  tags {
    Name       = "${var.environment_tag}-db-subnet-group"
    Deployment = "${var.deployment_method}"
  }
}

resource "aws_db_instance" "mysql" {
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7.16"
  multi_az               = true
  instance_class         = "db.t2.micro"
  port                   = "${var.db_port}"
  name                   = "${var.db_name}"
  username               = "${var.db_user}"
  password               = "${data.aws_ssm_parameter.db_password.value}"
  db_subnet_group_name   = "${aws_db_subnet_group.db_subnet_group.id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.sec.db_sg}"]
}

output "db_endpoint" {
  value = "${aws_db_instance.mysql.endpoint}"
}

resource "aws_ssm_parameter" "db_endpoint_ssm" {
  name        = "${var.environment_tag}-db-endpoint"
  description = "${var.environment_tag} db endpoint"
  type        = "SecureString"
  value       = "${aws_db_instance.mysql.endpoint}"

  tags {
    Name       = "${var.environment_tag}-db-endpoint"
    Deployment = "${var.deployment_method}"
  }
}